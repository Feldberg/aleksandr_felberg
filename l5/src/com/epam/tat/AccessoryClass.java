package com.epam.tat;

import java.util.ArrayList;
import java.util.Collection;

public class AccessoryClass {

    public static Figura returnFigura(String nameOfFigura, int side, int sideB ) {

        Figura figura = null;
        TypeOfFigura figuraName;

        switch (nameOfFigura) {
            case "triangle":
                figuraName = TypeOfFigura.TRIANGLE;
                figura = new Triangle( side );
                break;

            case "circle":
                figuraName = TypeOfFigura.CIRCLE;
                figura = new Circle( side );
                break;

            case "square":
                figuraName = TypeOfFigura.SQUARE;
                figura = new Square( side );
                break;

            case "rectangle":
                figuraName = TypeOfFigura.RECTANGLE;
                figura = new Rectangle( side, sideB );
                break;

            default:
                System.out.println( "It is wrong name of Figura!" );
        }

        return figura;
    }

    public static Collection<Figura> addFiguraToCollection(String[] args1) {

        Collection<Figura> arrayOfFiguras = new ArrayList<Figura>();

        for (int i = 0; i < args1.length; i++) {

            String stringOfMassiv = args1[i];
            String[] masForEveryinputString = stringOfMassiv.split( ":" );
            String nameOfFigura = masForEveryinputString[0];
            int side = Integer.parseInt( masForEveryinputString[1] );
            int sideB=0;
            if(masForEveryinputString.length ==3){
                sideB = Integer.parseInt( masForEveryinputString[2] );
            }
            arrayOfFiguras.add(returnFigura(nameOfFigura, side, sideB ));
            System.out.println(returnFigura(nameOfFigura, side, sideB ));

        }

        return arrayOfFiguras;
    }
}

package com.epam.tat;

public class Circle extends Figura {

    private int radius;
    String name = "Circle";

    public Circle(int radius) {
        this.radius = radius;
    }

    public String toString() {
        return  name + " - " + getRadius() + '\n' +
                "Area: " + area(radius) + '\n' +
                "Perimeter: " + perimeter(radius) + '\n';
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public static int area(int radius) {
        double areaOfCircle = Math.PI * radius * radius;
        return (int) areaOfCircle;
    }

    public static int perimeter(int radius) {
        double perimetrOfCircle = 2 * Math.PI * radius;
        return (int) perimetrOfCircle;
    }

}
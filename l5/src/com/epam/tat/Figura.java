package com.epam.tat;

public abstract class Figura implements Shape {

    private int side;
    String name = "Figura";

    public Figura() {
    }

    public Figura(int side) {
        this.side = side;
    }

    public String toString() {
        return  name + " - " + getSide() + '\n' +
                "Area: " + area(side) + '\n' +
                "Perimeter: " + perimeter(side) + '\n';
    }

    public int getSide() { return side; }

    public void setSide(int side) { this.side = side; }

    public static int area(int side) {
        return 0;
    }

    public static int area(int sideA, int sideB) {
        return 0;
    }

    public static int perimeter(int side) {
        return 0;
    }

    public static int perimeter(int sideA, int sideB) {
        return 0;
    }

}
package com.epam.tat;

public class Rectangle extends Figura {

    private int sideA;
    private int sideB;
    String name = "Rectangle";

    public Rectangle(int sideA, int sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public String toString() {
        return  name + " - " + getSideA() + " " + getSideB() + '\n' +
                "Area: " + area(sideA, sideB) + '\n' +
                "Perimeter: " + perimeter(sideA, sideB) + '\n';
    }

    public int getSideA() {
        return sideA;
    }

    public void setSideA(int sideA) {
        this.sideA = sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public void setSideB(int sideB) {
        this.sideB = sideB;
    }

    public static int area(int sideA, int sideB) {
        int areaOfRectangle = sideA * sideB;
        return areaOfRectangle;
    }

    public static int perimeter(int sideA, int sideB) {
        int perimetrOfRectangle = (sideA + sideB) * 2;
        return perimetrOfRectangle;
    }

}

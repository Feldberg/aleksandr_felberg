package com.epam.tat;

public class Square extends Figura {

    private int side;
    String name = "Square";

    public Square(int side) {
        this.side = side;
    }

    public String toString() {
        return  name + " - " + getSide() + '\n' +
                "Area: " + area(side) + '\n' +
                "Perimeter: " + perimeter(side) + '\n';
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public static int area(int side) {
        int areaOfSquare = side * side;
        return areaOfSquare;
    }

    public static int perimeter(int side) {
        int perimetrOfSquare = side * 4;
        return perimetrOfSquare;
    }

}

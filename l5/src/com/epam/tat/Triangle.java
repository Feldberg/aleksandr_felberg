package com.epam.tat;

public class Triangle extends Figura {

    private int side;
    String name = "Triangle";

    public Triangle(int side) {
        this.side = side;
    }

    public String toString() {
        return  name + " - " + getSide() + '\n' +
                "Area: " + area(side) + '\n' +
                "Perimeter: " + perimeter(side) + '\n';
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public static int area(int side) {
        double areaOfTriangle = side * side * Math.sqrt(3) / 4;
        return (int) areaOfTriangle;
    }

    public static int perimeter(int side) {
        int perimetrOfTriangle = side * 3;
        return perimetrOfTriangle;
    }

}

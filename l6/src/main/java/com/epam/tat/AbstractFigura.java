package com.epam.tat;

public abstract class AbstractFigura implements ShapeInterface {

    private static final int HASH_CONSTANT = 31;
    private String name = "Figura";
    private int side;
    private int areaFigura;
    private int perimetrFigura;

    public AbstractFigura() {
    }

    public AbstractFigura(int side) {
        this.side = side;
        this.areaFigura = calculateAreaOfFigura(side);
    }

    public AbstractFigura(int sideA, int sideB) {
        this.side = side;
        this.areaFigura = calculateAreaOfFigura(sideA, sideB);
    }

    public String toString() {
        return  name + " - " + getSide() + '\n'
                + "Area: " + areaFigura + '\n'
                + "Perimeter: " + perimetrFigura + '\n';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AbstractFigura figura = (AbstractFigura) o;

        return getAreaFigura() == figura.getAreaFigura() && name.equals(figura.name);
    }

    @Override
    public int hashCode() {
        int result = Integer.hashCode(areaFigura);
        result = HASH_CONSTANT * result + name.hashCode();
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAreaFigura() {
        return areaFigura;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public static int calculateAreaOfFigura(int side) {
        return 0;
    }

    public static int calculateAreaOfFigura(int sideA, int sideB) {
        return 0;
    }

    public static int calculatePerimeterOfFigura(int side) {
        return 0;
    }

    public static int calculatePerimeterOfFigura(int sideA, int sideB) {
        return 0;
    }

}

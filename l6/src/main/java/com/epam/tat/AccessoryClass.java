package com.epam.tat;

import java.util.*;

public class AccessoryClass {

    public static AbstractFigura returnFigura(String nameOfFigura, int side, int sideB) {

        AbstractFigura figura = null;
        TypeOfFigura figuraName;

        switch (nameOfFigura) {
            case "triangle":
                figuraName = TypeOfFigura.TRIANGLE;
                figura = new Triangle(side);
                break;

            case "circle":
                figuraName = TypeOfFigura.CIRCLE;
                figura = new Circle(side);
                break;

            case "square":
                figuraName = TypeOfFigura.SQUARE;
                figura = new Square(side);
                break;

            case "rectangle":
                figuraName = TypeOfFigura.RECTANGLE;
                figura = new Rectangle(side, sideB);
                break;

            default:
                System.out.println("It is wrong name of Figura!");
        }

        return figura;
    }

    public static Set<AbstractFigura> addFiguraToList(List<String> args1) {

        final int valueThree = 3;

        Set<AbstractFigura> setOfFiguras = new LinkedHashSet<>();

        //List<AbstractFigura> arrayOfFiguras = new ArrayList<AbstractFigura>();

        for (int i = 0; i < args1.size(); i++) {

            String stringOfMassiv = args1.get(i);
            List<String> masForEveryinputString = new ArrayList<String>();

            Collections.addAll(masForEveryinputString, stringOfMassiv.split(":"));
            String nameOfFigura = masForEveryinputString.get(0);
            int side = Integer.parseInt(masForEveryinputString.get(1));
            int sideB = 0;
            if (masForEveryinputString.size() == valueThree) {
                sideB = Integer.parseInt(masForEveryinputString.get(2));
            }
            AbstractFigura figura = returnFigura(nameOfFigura, side, sideB);
            setOfFiguras.add(figura);
        }

        return setOfFiguras;
    }
}

package com.epam.tat;

import java.util.Comparator;

public class AreaComporator implements Comparator<AbstractFigura> {

    //@Override
    public int compare(AbstractFigura o1, AbstractFigura o2) {
        if (o1.getAreaFigura() < o2.getAreaFigura()) {
            return 1;
        } else if (o1.getAreaFigura() > o2.getAreaFigura()) {
            return -1;
        }
        return 0;
    }

}
/*
class FirstNameComparator implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        // Для строк и некоторых других классов можно использовать метод compareTo.
        return o1.getFirstName().compareTo(o2.getFirstName());
    }
}

class AgeComparator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        // так короче
        // return o1.getAge() - o2.getAge();
        if (o1.getAge() > o2.getAge()) {
            return 1;
        } else if (o1.getAge() < o2.getAge()) {
            return -1;
        }
        return 0;
    }
}
*/

package com.epam.tat;

public class Circle extends AbstractFigura {

    String name = "Circle";
    private int radius;
    private int areaFigura;
    private int perimetrFigura;

    public Circle(int radius) {
        this.radius = radius;
        this.areaFigura = calculateAreaOfFigura(radius);
        this.perimetrFigura = calculatePerimeterOfFigura(radius);

    }

    public String toString() {
        return  name + " - " + getRadius() + '\n'
                + "Area: " + areaFigura + '\n'
                + "Perimeter: " + perimetrFigura + '\n';
    }

    @Override
    public int getAreaFigura() {
        return areaFigura;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public static int calculateAreaOfFigura(int radius) {
        return (int) (Math.PI * radius * radius);
    }

    public static int calculatePerimeterOfFigura(int radius) {
        return (int) (2 * Math.PI * radius);
    }
}

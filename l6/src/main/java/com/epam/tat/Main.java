package com.epam.tat;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {

        Set<AbstractFigura> hashSetOfFiguras = new  HashSet<AbstractFigura>();

        List<String> argsWithout0Element = new ArrayList<>();

        if (args[0].equals("--plain")) {
            for (int i = 1; i < args.length; i++) {
                argsWithout0Element.add(args[i]);
            }
            hashSetOfFiguras = AccessoryClass.addFiguraToList(argsWithout0Element);
        } else if (args[0].equals("--path")) {
            hashSetOfFiguras = AccessoryClass.addFiguraToList(readFigurasParamsFromFile(args[1]));
        }

        Set<AbstractFigura> linkedHashSetOfFiguras = sortAndPrintSetOfFiguras(hashSetOfFiguras);
    }

    public static Set<AbstractFigura> sortAndPrintSetOfFiguras(Set<AbstractFigura> setForSort) {
        Set<AbstractFigura> linkedHashSetOfFiguras = new LinkedHashSet<AbstractFigura>();
        List<AbstractFigura> listOfFiguras = new ArrayList<>();
        listOfFiguras.addAll(setForSort);
        Collections.sort(listOfFiguras, new AreaComporator());
        for (int i = 0; i < listOfFiguras.size(); i++) {
            linkedHashSetOfFiguras.add(listOfFiguras.get(i));
        }
        Iterator iterator = linkedHashSetOfFiguras.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        return linkedHashSetOfFiguras;
    }

    public static List<String> readFigurasParamsFromFile(String inputPath) {
        List<String> arrayOfInputString = new ArrayList<>();

        try {
            File file = new File(inputPath);
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String line = reader.readLine();

            while (line != null) {
                arrayOfInputString.add(line);
                line = reader.readLine();
            }
            fr.close();
            reader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return arrayOfInputString;
    }

}

package com.epam.tat;

public class Rectangle extends AbstractFigura {

    String name = "Rectangle";
    private int sideA;
    private int sideB;
    private int areaFigura;
    private int perimetrFigura;

    public Rectangle(int sideA, int sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.areaFigura = calculateAreaOfFigura(sideA, sideB);
        this.perimetrFigura = calculatePerimeterOfFigura(sideA, sideB);
    }

    public String toString() {
        return  name + " - " + getSideA() + " " + getSideB() + '\n'
                + "Area: " + areaFigura + '\n'
                + "Perimeter: " + perimetrFigura + '\n';
    }

    @Override
    public int getAreaFigura() {
        return areaFigura;
    }

    public int getSideA() {
        return sideA;
    }

    public void setSideA(int sideA) {
        this.sideA = sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public void setSideB(int sideB) {
        this.sideB = sideB;
    }

    public static int calculateAreaOfFigura(int sideA, int sideB) {
        return sideA * sideB;
    }

    public static int calculatePerimeterOfFigura(int sideA, int sideB) {
        return (sideA + sideB) * 2;
    }

}

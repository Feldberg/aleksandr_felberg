package com.epam.tat;

public class Square extends AbstractFigura {

    public static final int VALUE_FOUR = 4;
    String name = "Square";
    private int side;
    private int areaFigura;
    private int perimetrFigura;

    public Square(int side) {
        this.side = side;
        this.areaFigura = calculateAreaOfFigura(side);
        this.perimetrFigura = calculatePerimeterOfFigura(side);
    }

    public String toString() {
        return  name + " - " + getSide() + '\n'
                + "Area: " + areaFigura + '\n'
                + "Perimeter: " + perimetrFigura + '\n';
    }

    @Override
    public int getAreaFigura() {
        return areaFigura;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public static int calculateAreaOfFigura(int side) {
        return side * side;
    }

    public static int calculatePerimeterOfFigura(int side) {
        return side * VALUE_FOUR;
    }
}

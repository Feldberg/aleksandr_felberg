package com.epam.tat;

public enum TypeOfFigura {
    TRIANGLE, SQUARE, RECTANGLE, CIRCLE
}

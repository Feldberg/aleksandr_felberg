package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class BoyTest extends HumanTest{
    private Boy vasya;
    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        vasya = new Boy(Month.JULY,1000001.0,new Girl(true, false, vasya));
    }

    @Test(description = "If vasya is not reach, Month.JULY and vasya have pretty girl, " +
            "mood of Vasya is BAD", groups = {"Rich","Pretty", "Month"}, priority = 4)
    public void testGetMood() throws Exception {
        Mood actual = vasya.getMood();
        Assert.assertEquals(actual,Mood.EXCELLENT,"Invalid result mood of Boy.");
    }

    @DataProvider(name = "data for testSpendSomeMoney")
    public Object [][] dataForSpendMoney(){
        return new Object[][]{
                {10000.0,990001.0},
                {0.0, 1000001.0},
                {1000001.0, 0.0}
        };
    }

    @Test(description = "after spending 10000.0 boy1 will have 490000.0", dependsOnMethods = "testIsRich",
            groups = "Rich", dataProvider = "data for testSpendSomeMoney")
    public void testSpendSomeMoney (double sumSpendMoney, double expectedWealth) throws Exception {
        vasya.spendSomeMoney(sumSpendMoney);
        double actualWealth = vasya.getWealth();
        Assert.assertEquals(actualWealth,expectedWealth,"Invalid result summer month.");
    }

    @Test(description = "July is summer month", groups = "Month")
    public void testIsSummerMonth() throws Exception {
        boolean actual = vasya.isSummerMonth();
        Assert.assertEquals(actual,true,"Invalid result summer month.");
    }

    @Parameters({ "month" })
    @Test(description = "April is not summer month", groups = "Month")
    public void testIsSummerMonthWithParameters (Month month) throws Exception {
        boolean actual = vasya.isSummerMonth();
        Assert.assertEquals(actual,Month.APRIL,"Invalid result summer month.");
    }

    @Test(description = "vasya is not rich", groups = "Rich", priority = 1)
    public void testIsRich() throws Exception {
        boolean actual = vasya.isRich();
        Assert.assertEquals(actual,true,"Invalid result rich or not a Boy.");
    }

    @Test(description = "Girlfriend is pretty", groups = "Pretty", priority = 0)
    public void testIsPrettyGirlFriend() throws Exception {
        boolean actual = vasya.isPrettyGirlFriend();
        Assert.assertEquals(actual,true,"Invalid result pretty girl or not.");
    }

    @Test(description = "Vasya' Month.JULY", groups = "Month")
    public void testGetBirthdayMonth() throws Exception {
        Month actual = vasya.getBirthdayMonth();
        Assert.assertEquals(actual,Month.JULY,"Invalid result month of birthday a Boy.");
    }

    @Test(description = "Vasya have girlFriend", groups = "common", dependsOnGroups = "Rich", priority = 3)
    public void testGetGirlFriend() throws Exception {
        vasya.getGirlFriend();
        boolean actual = true;
        Assert.assertEquals(actual,true,"Invalid result Boy have girlFriend or not.");
    }

    @Test(description = "vasya wealth is 500000.0", groups = "Rich", priority = 2)
    public void testGetWealth() throws Exception {
        double actualWealth = 500000.0;
        Assert.assertEquals(actualWealth,500000.0,"Invalid result wealth of Boy");
    }
}

package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GirlTest extends HumanTest{
    private Girl lena;
    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        lena = new Girl(true, false, new Boy(Month.JULY,1000001.0, lena));
    }

    @Test(description = "If lena is pretty, have rich boyFriend, which is not fat " +
            "and buy new shoes for her, her Mood.EXCELLENT", groups = {"Pretty", "Rich", "Slim"})
    public void testGetMood() throws Exception {
        Mood actual = lena.getMood();
        Assert.assertEquals(actual,Mood.EXCELLENT,"Invalid result mood of Girl.");
    }

    @Test(description = "Lena's boyFriend can spend money fo her", groups = "Rich")
    public void testSpendBoyFriendMoney() throws Exception {
        lena.spendBoyFriendMoney(10000.0);
        boolean actual = true;
        Assert.assertEquals(actual,true,"Invalid result Boy can spend money for Girl or not");
    }

    @Test(description = "Lena's boyFriend is reach so far", groups = "Rich")
    public void testIsBoyfriendRich() throws Exception {
        boolean actual = lena.isBoyfriendRich();
        Assert.assertEquals(actual,true,"Invalid result Girl's Boy is rich or not");
    }

    @Test(description = "Lena will get new shoes", groups = {"Rich", "Pretty"})
    public void testIsBoyFriendWillBuyNewShoes() throws Exception {
        boolean actual = lena.isBoyFriendWillBuyNewShoes();
        Assert.assertEquals(actual,true,"Invalid result Girl get new shoes or not");
    }

    @Test(description = "Lena's boyFriend is slim so far", groups = {"Slim", "Pretty"})
    public void testIsSlimFriendBecameFat() throws Exception {
        boolean actual = lena.isSlimFriendBecameFat();
        Assert.assertEquals(actual,false,"Invalid result Slim friend became fat or not");
    }

    @Test(description = "Lena is pretty", groups = "Pretty")
    public void testIsPretty() throws Exception {
        boolean actual = lena.isPretty();
        Assert.assertEquals(actual,true,"Invalid result girl is pretty or not");
    }

    @Test(description = "Lena have boyFriend", groups = "common", dependsOnGroups = "Rich", alwaysRun = true)
    public void testGetBoyFriend() throws Exception {
        lena.getBoyFriend();
        boolean actual = true;
        Assert.assertEquals(actual,true,"Invalid result girl is pretty or not");
    }

    @Test(description = "Slim Friend of Lena did not get a few kilos", groups = {"Slim", "Pretty"})
    public void testIsSlimFriendGotAFewKilos() throws Exception {
        boolean actual = lena.isSlimFriendGotAFewKilos();
        Assert.assertEquals(actual,false,"Invalid result Slim Friend Got A Few Kilos or not");
    }
}

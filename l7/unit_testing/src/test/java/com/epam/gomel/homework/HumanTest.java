package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class HumanTest {
    private Human human;

    @DataProvider(name = "data for testGetMood Human")
    public Object [][] dataForGetMoodHuman(){
        return new Object[][]{
                {human = new Boy(Month.MARCH,1100000.0, new Girl(true, true)), Mood.GOOD},
                {human = new Boy(Month.JULY,1100000.0, new Girl(true, true)), Mood.EXCELLENT},
                {human = new Boy(Month.JUNE,1500000.0), Mood.NEUTRAL},
                {human = new Boy(Month.AUGUST), Mood.BAD},
                {human = new Boy(Month.JANUARY), Mood.HORRIBLE},
                {human = new Girl(true, true, new Boy(Month.JUNE,11000000)), Mood.EXCELLENT},
                {human = new Girl(false, true), Mood.NEUTRAL},
                {human = new Girl(true), Mood.GOOD},
                {human = new Girl(false), Mood.I_HATE_THEM_ALL},
                {human = new Girl(), Mood.NEUTRAL},
        };
    }

    @Test(description = "Verify getMoodTest" , groups = {"Pretty", "Rich", "Slim"},
            dataProvider = "data for testGetMood Human")
    public void getMoodTest(Human smthHuman, Mood smthMood){
        human = smthHuman;
        Mood actual = smthHuman.getMood();
        Assert.assertEquals(actual, smthMood);
    }
}

package com.epam.gomel.homework;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class TestRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList(
                "./src/test/resources/suites/testng.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}
